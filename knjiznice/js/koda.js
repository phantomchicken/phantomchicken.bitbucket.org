
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
 var Uporabnik1={
   ime: "Homer",
   priimek: "Simpson",
   spol:"MALE",
   naslov: "Evergreen Terrace 742",
   datum: "1979-12-05",
   nasicenost: 90,
   sistola: 160,
 };
 var Uporabnik2={
   ime: "Lois",
   priimek: "Griffin",
   spol:"FEMALE",
   naslov: "Spooner Street 31",
   datum: "1977-02-06",
   nasicenost: 96,
   sistola: 113,
 };
 var Uporabnik3={
   ime: "Emmett",
   priimek: "Brown",
   spol:"MALE",
   naslov: "Riverside Drive 1640",
   datum: "1950-01-03",
   nasicenost: 86,
   sistola: 139,
 }
 var uporabniki =[Uporabnik1,Uporabnik2,Uporabnik3];
 var counter=0;
 var zeGenerirani=false;
 
function generirajPodatke(stPacienta) {
  if (!zeGenerirani){
 var uporabnik;
 switch (stPacienta) {
   case 1:
     uporabnik = uporabniki[0];
     break;
   case 2:
     uporabnik = uporabniki[1];
     break;
   case 3:
     uporabnik = uporabniki[2];
     break;
 }
 //console.log(uporabnik);
  var forma = document.getElementById("preberiIme");
  var opt = document.createElement("option");
  opt.value = stPacienta;
  //console.log(opt.value);
  opt.text = uporabnik.ime+ " "+uporabnik.priimek;
  forma.add(opt);

 //console.log(uporabnik.ime);
 if (!uporabnik.ime || !uporabnik.priimek || !uporabnik.datum || uporabnik.ime.trim().length == 0 ||
      uporabnik.priimek.trim().length == 0 || uporabnik.datum.trim().length == 0) {
		console.alert("Prosimo vnesite validne podatke!")
	} else {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (data) {
        var ehrId = data.ehrId;
        uporabnik["ehrId"]=ehrId;
        var partyData = {
          firstNames: uporabnik.ime,
          lastNames: uporabnik.priimek,
          dateOfBirth: uporabnik.datum,
          gender: uporabnik.spol,
          additionalInfo: {"ehrId": ehrId, "naslov": uporabnik.naslov, "spol":spol,
          "nasicenost":uporabnik.nasicenost, "sistolicniTlak":uporabnik.sistola, "letoRojstva":datum}
          };
        //console.log(ehrId);
      },
      error: function(err) {
          	console.alert("Prosimo vnesite validne podatke!")
      }
		});
	}
  }
  counter++;
  if (counter==3) zeGenerirani=true;
}

window.addEventListener('load', function() {
    var vrednostIzbireTEST = document.getElementById("poljeIme").value="";
    var vrednostIzbireTEST = document.getElementById("poljePriimek").value="";
    var vrednostIzbireTEST = document.getElementById("poljeNaslov").value="";
    var vrednostIzbireTEST = document.getElementById("poljeLetoRojstva").value="";
    var vrednostIzbireTEST = document.getElementById("poljeSpol").value="";
    var vrednostIzbireTEST = document.getElementById("poljeEHR").value="";
    var vrednostIzbireTEST = document.getElementById("preberiEHR").value="";
    var vrednostIzbireTEST = document.getElementById("poljeNasicenostKrviSKisikom").value="";
    var vrednostIzbireTEST = document.getElementById("poljeKrvniTlakSistolicni").value="";
    var vrednostIzbire=document.getElementById("preberiEHR");
    vrednostIzbire.innerText = vrednostIzbire.value;
});  

var trenutenEHRId=null;


function preberiIme() {
  var vrednostIzbire = document.getElementById("preberiIme").value;
  if (vrednostIzbire.length==0 || vrednostIzbire==null){
    window.alert("Prosimo izberite bolnika!");
  } else {
  var trenutenEHRId= uporabniki[vrednostIzbire-1].ehrId;
  console.log("trenutni EHR je "+trenutenEHRId);
      var ix=0;
      while(ix<3){
        var uporabnik = uporabniki[ix];
        if(uporabnik.ehrId == uporabniki[vrednostIzbire-1].ehrId) {
            console.log(uporabnik);
            var ime = document.getElementById("poljeIme");
            ime.value = uporabnik.ime;
            var priimek = document.getElementById("poljePriimek");
            priimek.value = uporabnik.priimek;
            var spol = document.getElementById("poljeSpol");
            spol.value = uporabnik.spol;
            var naslov = document.getElementById("poljeNaslov");
            naslov.value = uporabnik.naslov;
            var ehrId = document.getElementById("poljeEHR");
            ehrId.value = uporabnik.ehrId;
            var letoRojstva = document.getElementById("poljeLetoRojstva");
            letoRojstva.value = uporabnik.datum;
            var nasicenost = document.getElementById("poljeNasicenostKrviSKisikom");
            nasicenost.value = uporabnik.nasicenost;
            var sistolicniTlak = document.getElementById("poljeKrvniTlakSistolicni");
            sistolicniTlak.value = uporabnik.sistola;
            obkrozi();
            break;
        }
        ix++;
    }
  }
  
}
  
function preberiEHR(){
    var izbira=document.getElementById("preberiEHR");
    var vrednostIzbire =izbira.value;
    var neObstaja = false;
    console.log(vrednostIzbire);
    console.log(uporabniki);
    for (var i=0;i<uporabniki.length;i++){
        console.log(uporabniki[i].ehrId);
        if (uporabniki[i].ehrId==vrednostIzbire){
            console.log("DING");
            var ime = document.getElementById("poljeIme");
            ime.value = uporabniki[i].ime;
            var priimek = document.getElementById("poljePriimek");
            priimek.value = uporabniki[i].priimek;
            var spol = document.getElementById("poljeSpol");
            spol.value = uporabniki[i].spol;
            var naslov = document.getElementById("poljeNaslov");
            naslov.value = uporabniki[i].naslov;
            var ehrId = document.getElementById("poljeEHR");
            ehrId.value = uporabniki[i].ehrId;
            var letoRojstva = document.getElementById("poljeLetoRojstva");
            letoRojstva.value = uporabniki[i].datum;
            var nasicenost = document.getElementById("poljeNasicenostKrviSKisikom");
            nasicenost.value = uporabniki[i].nasicenost;
            var sistolicniTlak = document.getElementById("poljeKrvniTlakSistolicni");
            sistolicniTlak.value = uporabniki[i].sistola;
            obkrozi();
            break;
        }
        if (i==uporabniki.length-1){
            window.alert("Ta EHR ne obstaja! Poskusite znova");
            neObstaja=true;
        }
    }
}
    
    var barve =[];
    var moski = [];
    var letaMoska = [];
    
function tabelaIzJSONA() {
       $.getJSON("knjiznice/json/tlak.json", function(data) {
       //console.log(data); 
       console.log(data.fact);
       //console.log(data.fact.length);
       var tabela=[];
       var podatki =[];
       var leta=[];
       var pol=[];
       for (var i=0;i<data.fact.length;i++){
           tabela[i] = data.fact[i];
           leta[i] = parseInt(tabela[i].dims.YEAR.substring(0,4));
           pol[i] = tabela[i].dims.SEX.substring(0,6);
           podatki[i] = parseFloat(tabela[i].Value.substring(0,5));
       }
       console.log(leta);
       console.log(pol);
       console.log(podatki);
       var moski = [];
       var letaMoska = [];
       var letaZenska = [];
       var zenski = [];
       var stMoski=0;
       var stZenski=0;
       var barve =[];
       for (var i=0;i<podatki.length;i++){
           if (i%5==0) barve[i] = ["#3e95cd"];
           if (i%5==1) barve[i]=["#8e5ea2"];
           if(i%5==2) barve[i] = ["#3cba9f"];
           if (i%5==3) barve[i]=["#e8c3b9"];
           if (i%5==4) barve[i]=["#c45850"];
           if (pol[i]=="Male"){
               moski[stMoski]=podatki[i];
               letaMoska[stMoski]=leta[i];
               stMoski++;
           } else {
               zenski[stZenski]=podatki[i];
               letaZenska[stZenski] =leta[i];
               stZenski++;
           }
       }
       console.log(barve);
       var canvas = document.getElementById("myChart");
       var canvas2 = document.getElementById("myChart2");
       var spol = document.getElementById("poljeSpol");
        console.log(spol.value);
       
        if(spol.value=="MALE"){
            canvas.style.visibility="visible";
            canvas2.style.visibility="hidden";
        var barChart = Chart.Bar (canvas,{
              data: {
              labels: letaMoska,
              datasets: [
                {
                  label: "Mean systolic blood pressure (age-standardized estimate)",
                  backgroundColor: barve,
                  data: moski,
                }
              ]
            },
            options: {
              legend: { display: false },
              title: {
                display: true,
                text: 'Moški sistolični srčni pritisk skozi leta'
              }
            }
            });
            
            var barChart2 = Chart.Bar (canvas2,{
              data: {
              labels: letaZenska,
              datasets: [
                {
                  label: "Mean systolic blood pressure (age-standardized estimate)",
                  backgroundColor: barve,
                  data: zenski,
                }
              ]
            },
            options: {
              legend: { display: false },
              title: {
                display: true,
                text: 'Ženski sistolični srčni pritisk skozi leta'
              }
            }
            });
    
       
        } else if (spol.value=="FEMALE") {    
        
        var barChart = Chart.Bar (canvas,{
              data: {
              labels: letaMoska,
              datasets: [
                {
                  label: "Mean systolic blood pressure (age-standardized estimate)",
                  backgroundColor: barve,
                  data: moski,
                }
              ]
            },
            options: {
              legend: { display: false },
              title: {
                display: true,
                text: 'Moški sistolični srčni pritisk skozi leta'
              }
            }
            });
        
        var barChart2 = Chart.Bar (canvas2,{
              data: {
              labels: letaZenska,
              datasets: [
                {
                  label: "Mean systolic blood pressure (age-standardized estimate)",
                  backgroundColor: barve,
                  data: zenski,
                }
              ]
            },
            options: {
              legend: { display: false },
              title: {
                display: true,
                text: 'Ženski sistolični srčni pritisk skozi leta'
              }
            }
            });
            canvas.style.visibility="hidden";
        canvas2.style.visibility="visible";
        } else {
            window.alert("Napaka, uporabnik ni izbran!");
        }
       });
       return;
   } 

function obkrozi (){
    var tlak = document.getElementById("poljeKrvniTlakSistolicni").value;
    var spol = document.getElementById("poljeSpol").value;
    var starost = document.getElementById("poljeLetoRojstva").value.substring(0,4);
    starost = 2019-starost;
    console.log(tlak);
    console.log(spol);
    console.log(starost);
    if (starost==40){
     if (spol=="MALE"){
         $('#visok').css({
            'background-color': 'red',
        'color': 'white',
        });
     }   
    } else {
        $('#visok').css({
            'background-color': 'white',
        'color': 'black',
        });
    }
    
    if (tlak>=140){
        document.getElementById("rezultat").innerHTML = "Visok!";
       setTimeout( function yea( ) { alert( "Pozor! Visok krvni tlak. Nasveti: https://www.aktivni.si/zdravje/svetovni-dan-hipertenzije-10-nasvetov-za-zdrav-krvni-tlak/ " ); }, 2000 );
        $('#rezultat').css({
        'color': 'red',
        });
        $('#vecKot140').css({
            'background-color': 'red',
        'color': 'white',
        });
        $('#med120').css({
            'background-color': 'white',
        'color': 'black',
        });
        $('#manjKot120').css({
            'background-color': 'white',
        'color': 'black',
        });
    } else if (tlak>120 && tlak<140){
        document.getElementById("rezultat").innerHTML = "Srednji!";
        $('#rezultat').css({
        'color': 'orange',
        });
        $('#med120').css({
            'background-color': 'orange',
        'color': 'white',
        });
        $('#vecKot140').css({
            'background-color': 'white',
        'color': 'black',
        });
        $('#manjKot120').css({
            'background-color': 'white',
        'color': 'black',
        });
    } else if (tlak<120){
        document.getElementById("rezultat").innerHTML = "Normalen!";
        $('#rezultat').css({
        'color': 'green',
        });
        $('#manjKot120').css({
            'background-color': 'green',
        'color': 'white',
        });
        $('#vecKot140').css({
            'background-color': 'white',
        'color': 'black',
        });
        $('#med120').css({
            'background-color': 'white',
        'color': 'black',
        });
    }
}



function spremeniPodatke() {
    var spol = document.getElementById("poljeSpol");
    console.log(spol.value);
    var canvas = document.getElementById("myChart");
    var canvas2 = document.getElementById("myChart2");
    if (spol.value=="MALE"){
        canvas.style.visibility="hidden";
        canvas2.style.visibility="visible";
    } else if (spol.value=="FEMALE"){
        console.log("usao");
        canvas.style.visibility="visibile";
        canvas2.style.visibility="hidden";
    } else {
        window.alert("Napaka, uporabnik ni izbran!");
    }
}